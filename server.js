var express    = require('express')
var cors = require('cors')
var mysql = require('mysql');
var session = require('express-session');
var bodyParser = require('body-parser');
var md5 = require('md5');
const http = require('http');
const path = require('path');
var app = express()
const Moment = require('moment');
var cron = require('node-cron');
// const Nexmo = require('nexmo');
const way2sms = require('way2sms');

const port  = process.env.PORT  ||  8081;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({secret:'Coreco'}));
app.use(express.static(__dirname + '/dist'));

/**---------------------------to send sms to caterer for lunch box count---------------------  */
/* const nexmo = new Nexmo({
     apiKey: '956fb8a7',
    apiSecret: 'GLHExXV2xYsrpwkl'
  })*/

var con = mysql.createConnection({
    host: "localhost",  
 user: "root",  
 password: "smd123", 
 database: 'crc_emp_mgmt',
 port: '3306'
});

con.connect(function(err)
        {
	        if (err) throw err;
            console.log("connection created..!!");
        });

    module.exports = con;  //it is export to include this variable/db connection in another js files  
     
    //used to call angular ,the build project in dist folder
    app.use(express.static(__dirname + '/dist'));   

    const common_actions = require('./models/v1/common_actions'); //this file has common functionalities for all admin,employees
    app.use('/ems/v1/common_actions', common_actions);

   
    const admin_actions = require('./models/v1/admin'); //this file has  functionalities for only admin
    app.use('/ems/v1/admin_actions', admin_actions);


    const employee_actions = require('./models/v1/employee'); //this file has  functionalities for only employees
    app.use('/ems/v1/employee_actions', employee_actions);

    const lunch_box = require('./models/v1/lunch_box'); //functionalities of lunch box app.
    app.use('/ems/v1/LunchBox',lunch_box);


    //updated on 9 oct for std mean stack flow mvc,nodejs controller calls to angular
    app.get('/*',(req,res) => res.sendFile(path.join(__dirname,'/dist/index.html'))); 
     
    // Cron Jobs 
    /**********************CRON for reset monthly availed leaves ********************* */
    cron.schedule('5 0 1 * *', () => {
        sql=`UPDATE crc_emp_leave_details SET emp_leaves_availed_this_month='0' WHERE company_id='1'`;
        con.query(sql,function(err,result){
            if (err) throw err;
            if(result!=null){
                console.log('monthly availed leaves are reset');
            }         
        })
      });

      /**********************CRON for reset yearly availed leaves ********************* */
      cron.schedule('15 1 31 12 *', () => {
        sql=`UPDATE crc_emp_leave_details SET emp_leaves_availed_this_fin_year='0' WHERE company_id='1'`;
        con.query(sql,function(err,result){
            if (err) throw err;
            if(result!=null){
                console.log('monthly availed leaves are reset');
            }         
        })
      });

      /**********************CRON for send daily tiffin box ********************* */

          var today = new Date();
          var todays_date = today.toISOString().slice(0,10);
          console.log(todays_date);
        /**---------------------------to get default tiffin count---------------------  */
       sql1='SELECT * FROM company_master INNER JOIN crc_caterer_master ON company_master.company_id=crc_caterer_master.company_id  WHERE company_master.company_id=1';
       con.query(sql1,function(err,result){
        var nlb_count;
        var leave_count;
        var tiffin_count;
        var todays_tiffin;
        var fasting;
        var caterer_mobile_number;

           if(err) throw err;
           tiffin_count = result[0].company_lunch_box_count;
           caterer_mobile_number=result[0].caterer_mobile_number;

            /**---------------------------to get emp don't want lunch---------------------  */
           sql2=`SELECT emp_mobile_number FROM crc_emp_lunchbox_details where FIND_IN_SET('${todays_date}','emp_nlb_dates')`;
           con.query(sql2,function(err,result){
               if (err) throw err;
               nlb_count = result.length;

               /**---------------------------to get emp on leave---------------------  */

               sql3=`SELECT emp_mobile_number FROM crc_emp_lunchbox_details where FIND_IN_SET('${todays_date}','emp_leave_dates')`;
               con.query(sql3,function(err,result){
                   if(err) throw err;
                   leave_count=result.length;
                   console.log('leave count'+leave_count);

                     /**---------------------------to get emp having fast---------------------  */

                   sql4=`SELECT emp_mobile_number FROM crc_emp_lunchbox_details where FIND_IN_SET('${todays_date}','emp_fast_dates')`;
                   con.query(sql4,function(err,result){
                       if (err) throw err;
                       fasting=result.length;
                       console.log('fasting'+fasting);

                       todays_tiffin=tiffin_count-leave_count-nlb_count;
                        console.log('todays tiffine'+todays_tiffin);
                        /*const from = 'Nexmo'
                         const to = `${caterer_mobile_number}`;
                        //const to = '918308896183';
                        const text = `total tiffin=${todays_tiffin},Emp having Fast=${fasting}`;
                        console.log('cron executed');
                          nexmo.message.sendSms(from, to, text);*/
                         console.log(todays_tiffin,fasting,caterer_mobile_number);
                        sendSms(todays_tiffin,fasting,caterer_mobile_number,todays_date);
                   });

               });

           });
           
       });
       
      });
    

      async function sendSms(todays_tiffin,fasting,caterer_mobile_number,todays_date){
        cookie = await way2sms.login('8308896183', 'shubh28');
        await way2sms.send(cookie,`${caterer_mobile_number}`,`Lunchboxes @ CoReCo Technologies Date:${todays_date} Veg:=${todays_tiffin},Fast:${fasting},Egg:00 Contact-9595280870`);
      }
	const server = http.createServer(app);
    server.listen(port, () => console.log('Running at 8081'));