var express    = require('express');
const router = express.Router();
var con = require('../../server');  //db connection from server.js file

router.post('/GetEmployeeLeaveDates',function(req,res){
    var employee_mobile_number = req.body[0].emp_mobile_number;
    var company_id = req.body[0].company_id;
    var arr = [];
    console.log('In method');
    sql = `SELECT * FROM crc_emp_lunchbox_details INNER JOIN crc_emp_leave_details ON crc_emp_leave_details.emp_mobile_number=crc_emp_lunchbox_details.emp_mobile_number  WHERE crc_emp_lunchbox_details.emp_mobile_number ='${employee_mobile_number}' AND crc_emp_lunchbox_details.company_id ='${company_id}'`;
    con.query(sql,function(err,result){
        if(err){
            throw err;
        }
        arr.push({'emp_total_remaining_leaves':result[0].emp_total_remaining_leaves,'emp_leaves_availed_this_month':result[0].emp_leaves_availed_this_month,'emp_leaves_availed_this_fin_year':result[0].emp_leaves_availed_this_fin_year});
       var emp_leave_dates = result[0].emp_leave_dates;
       var emp_nlb_dates = result[0].emp_nlb_dates;
       var emp_fast_dates = result[0].emp_fast_dates;
        console.log('query executed'+result[0].company_id);
       
        var emp_leave_dates_array = emp_leave_dates.split(",");
       for(var i=0;i<emp_leave_dates_array.length;i++){
        //    arr.push({'1':emp_leave_dates_array[i]});
        var date=emp_leave_dates_array[i];
           arr.push({[date]:1});
       }

       var emp_nlb_dates_array = emp_nlb_dates.split(",");
       for(var i=0;i<emp_nlb_dates_array.length;i++){
        var date=emp_nlb_dates_array[i];
           arr.push({[date]:2});
       }

       var emp_fast_dates_array = emp_fast_dates.split(",");
       for(var i=0;i<emp_fast_dates_array.length;i++){
        var date=emp_fast_dates_array[i];
           arr.push({[date]:3});
       }
       var resp_json = [{'emp_leave_details':arr}]
       res.send(resp_json);   
    })
})

router.post('/CancelLunchbox',function(req,res){
    var absent_dates_json= req.body[0].absent_dates;
    var leave_type = req.body[0].leave_type;
    var emp_mobile_no = req.body[0].emp_mobile_no;
    var emp_company_id = req.body[0].emp_company_id;
    var emp_leave_dates = '';
    var emp_fast_dates = '';
    var emp_no_lunchbox_dates = '';

   // var myJson = {'key':'value', 'key2':'value2'};
    for(var myKey in absent_dates_json) {
    console.log("key:"+myKey+", value:"+absent_dates_json[myKey]);
    switch(absent_dates_json[myKey]){
        case 1:
        //case 1 means cancel the lunchbox.
        //as well as increase the emp leave count.
        //echo "Emp On Leave";
        if(emp_leave_dates.length>0){
            emp_leave_dates=emp_leave_dates+","+myKey;
        }else{
            emp_leave_dates=myKey;
        }
        break;

        case 2:
        //case = 2 means cancel only lunch box.
        if(emp_no_lunchbox_dates.length>0){
            emp_no_lunchbox_dates=emp_no_lunchbox_dates+","+myKey
        }else{
            emp_no_lunchbox_dates=myKey;
        }
        break;

        case 3:
        //leave type =  3 means emp has fast.
        if(emp_fast_dates.length>0){
            emp_fast_dates=emp_fast_dates+","+myKey;
        }else{
            emp_fast_dates=myKey;
        }
        break;
    }
    }
    var recordFlag;
    sql=`SELECT * FROM crc_emp_lunchbox_details WHERE emp_mobile_number='${emp_mobile_no}'`;
    con.query(sql,function(err,result){
        if(err) throw err;
        
        recordFlag=result.length;
        console.log("record flag is="+recordFlag);
        console.log("emp absent dates"+emp_leave_dates);
        console.log("emp fast days"+emp_fast_dates);
        console.log("emp no lmb "+emp_no_lunchbox_dates);
        if(recordFlag==0){
            sql2=`INSERT INTO crc_emp_lunchbox_details(company_id,emp_mobile_number,emp_leave_dates,emp_nlb_dates,emp_fast_dates,emp_leave_type) VALUES ('${emp_company_id}','${emp_mobile_no}','${emp_leave_dates}','${emp_no_lunchbox_dates}','${emp_fast_dates}','${leave_type}')`;
            con.query(sql2,function(err,result){
                if (err) throw err;
                if(result!=null){
                    res.send([{"response_code":1,"mobile_number":emp_mobile_no}]);
                }
            })
        }
        else{
            sql1=`UPDATE crc_emp_lunchbox_details SET emp_leave_dates='${emp_leave_dates}',emp_nlb_dates='${emp_no_lunchbox_dates}',emp_fast_dates='${emp_fast_dates}',emp_leave_type='${leave_type}' WHERE emp_mobile_number='${emp_mobile_no}' AND company_id='${emp_company_id}'`;
            con.query(sql1,function(err,result){
                if (err) throw err;
                if(result!=null){
                    res.send([{"response_code":1,"mobile_number":emp_mobile_no}]);
                }
            })
        }
    })
})

module.exports = router;