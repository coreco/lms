var express    = require('express');
const router = express.Router();

var md5 = require('md5');
var con = require('../../server');   //db connection from server.js file





/************************************LOGIN****************************** */
router.post('/auth',function(req,res){
     
    sql=`SELECT * FROM crc_emp_details INNER JOIN crc_role_master ON crc_emp_details.emp_role=crc_role_master.role_id  WHERE emp_mobile_number='${req.body[0].phone}'`;
    con.query(sql,function(err,result){
        if (err) {
            console.warn(err);
            res.send({'success':'DB Connection Error'});
          }else if(result == ""){
            res.send([{'response_code':2,
                    'employee_id':'',
                    'employee_company_id':'',
                    'employee_first_name':'',
                    'employee_last_name':'',
                    'mobile_number':`${req.body[0].phone}`,
                    'employee_email':'',
                    'employee_is_vegetarian':'',
                    'employee_is_eggetarian':'',
                    'employee_is_non_vegetarian':'',
                    'is_admin':'',
                    'message':'Login unsuccessful',
                    'success':'false',                
                   }]);
          }else if(result != ""){            
            console.log(req.body[0]);
            var pass=(md5(req.body[0].password));
           
            result.forEach(function(data){
                if(data.emp_passcode==pass){
                   
                    sess = req.session;
                 
                    var employee_first_name = result[0].emp_first_name;
                    var employee_last_name = result[0].emp_last_name;
                    var employee_email = result[0].emp_email;
                    var mobile_number = result[0].emp_mobile_number;
                    var employee_is_vegetarian = result[0].emp_is_vegetarian;
                    var employee_is_eggetarian = result[0].emp_is_eggetarian;
                    var employee_is_non_vegetarian = result[0].emp_is_non_vegetarian;
                    var is_admin = result[0].emp_role;
                    var status = result[0].emp_account_status;
                   console.log("Acc status is"+status);
                    var employee_id = result[0].emp_mobile_number;
                    var employee_company_id = result[0].company_id;

                    var name = employee_first_name + ' ' + employee_last_name;
                    
                     sess.req.body[0].phone;
                     sess.emp_first_name = employee_first_name;
                     sess.emp_last_name = employee_last_name;
                     sess.emp_email = employee_email;
                     sess.emp_mobile_number = mobile_number;
                     sess.name = name;
                   
                     if(status!=1){
                        res.send([{'response_code':-1,
                        'employee_id':`${employee_id}`,
                        'employee_company_id':`${employee_company_id}`,
                        'employee_first_name':`${employee_first_name}`,
                        'employee_last_name':`${employee_last_name}`,
                        'mobile_number':`${employee_id}`,
                        'employee_email':`${employee_email}`,
                        'employee_is_vegetarian':`${employee_is_vegetarian}`,
                        'employee_is_eggetarian':`${employee_is_eggetarian}`,
                        'employee_is_non_vegetarian':`${employee_is_non_vegetarian}`,
                        'is_admin':`${is_admin}`,
                        'message':'Login successful',
                        'success':'true',
                        'logas':`${data.role_name}`,
                        'name':name}]);
                     }else{
                        res.send([{'response_code':1,
                    'employee_id':`${employee_id}`,
                    'employee_company_id':`${employee_company_id}`,
                    'employee_first_name':`${employee_first_name}`,
                    'employee_last_name':`${employee_last_name}`,
                    'mobile_number':`${employee_id}`,
                    'employee_email':`${employee_email}`,
                    'employee_is_vegetarian':`${employee_is_vegetarian}`,
                    'employee_is_eggetarian':`${employee_is_eggetarian}`,
                    'employee_is_non_vegetarian':`${employee_is_non_vegetarian}`,
                    'is_admin':`${is_admin}`,
                    'message':'Login successful',
                    'success':'true',
                    'logas':`${data.role_name}`,
                    'name':name}]);
                     }
                    
                }else{
                    res.send([{'response_code':2,
                    'employee_id':'',
                    'employee_company_id':'',
                    'employee_first_name':'',
                    'employee_last_name':'',
                    'mobile_number':`${result[0].emp_mobile_number}`,
                    'employee_email':'',
                    'employee_is_vegetarian':'',
                    'employee_is_eggetarian':'',
                    'employee_is_non_vegetarian':'',
                    'is_admin':'',
                    'message':'Login unsuccessful',
                    'success':'false',                
                   }]);
                }
            })
          }
        
    })

    });
    


/*---------------------------Change Password-------------------------*/

router.post('/cngpass',function(req,res){
    console.log(req.body[0]);
    sql=`SELECT * FROM crc_emp_details WHERE emp_mobile_number='${req.body[0].phone}'`;
    con.query(sql,function(err,result){
        if(err) throw err;
        //console.log(result);
       // console.log('Old pass=='+req.body[0].old);
        var oldpass=md5(req.body[0].old);
        var newpass=md5(req.body[0].new);
       // console.log(newpass);
        result.forEach(function(data){
            if(data.emp_passcode==oldpass){ 
                sql2=`UPDATE crc_emp_details SET emp_passcode='${newpass}' WHERE emp_mobile_number='${req.body[0].phone}'`;
                con.query(sql2,function(err,result){
                    if(err) throw err;
                    res.send({'success':true});
                })
            }else{
                res.send({'success':false});
            }
        })
    });
})

/*---------------------------check current Password-------------------------*/

router.post('/checkPassword',function(req,res){
 
    var check_current_pass =md5(req.body[0].check_current_pass);


    sql=`SELECT * FROM crc_emp_details WHERE emp_mobile_number='${req.body[0].phone}' and emp_passcode = '${check_current_pass}'`;
    con.query(sql,function(err,result){

        
        if (err) {
            console.warn(err);
            res.send({'success':'DB Connection Error'});
          }else if(result == ""){
            
            res.send({'success':true});
          }else if(result != ""){
           
            res.send({'success':false});         
        }
        
    })


})


/*------------------------- Logout---------------------------- */
router.get('/Logout',function(req,res){
    req.session.destroy(function(err){
        if(err) throw err;
        res.send({success:true});
    });
});




module.exports = router;


/*
var express    = require('express');
const router = express.Router();
var cors = require('cors')
var mysql = require('mysql');
var session = require('express-session');
var bodyParser = require('body-parser');
var md5 = require('md5');
const http = require('http');
const path = require('path');
var app = express();
var con = require('../server');   //db connection from server.js file


app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({secret:'Coreco'}));
app.use(express.static(__dirname + '/dist/corecov2'));



*/