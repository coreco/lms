var express    = require('express');
const router = express.Router();

var md5 = require('md5');
var app = express();
var con = require('../server');   //db connection from server.js file






/*-------------------Leave Request----------------------------- */
router.post('/leave',function(req,res){

    var listDate = [];
    var startDate =req.body[0].fdate;
    var endDate = req.body[0].tdate;
    var dateMove = new Date(startDate);
    var strDate = startDate;
    
    while (strDate < endDate){
      var strDate = dateMove.toISOString().slice(0,10);
      listDate.push(strDate);
      dateMove.setDate(dateMove.getDate()+1);
    };
    //console.log(listDate);
    let strdates=listDate.toString();
    console.log(strdates);
    var sql=`INSERT INTO crc_emp_leave_requests(emp_mobile_number,emp_leave_applied_date,emp_leave_start_date,emp_leave_end_date,emp_leave_type,emp_leave_reason,emp_leave_status,emp_leave_number_of_days,company_id,emp_manager_remark) VALUES('${req.body[0].phone}','${req.body[0].apdate}','${req.body[0].fdate}','${req.body[0].tdate}','${req.body[0].type}','${req.body[0].reason}','${req.body[0].status}','${req.body[0].leavedays}',1,'--');`;
            con.query(sql,function(err,result){
                if(err) throw err;
               
            });
    
    var sql1=`SELECT * FROM crc_emp_lunchbox_details WHERE emp_mobile_number=${req.body[0].phone}`;
    con.query(sql1,function(err,result){
        if(err) throw err;
        if(result.affectedRows){
            console.log('in lb if');
            var sql2=`UPDATE crc_emp_lunchbox_details SET emp_leave_dates='${strdates}' WHERE emp_mobile_number='${req.body[0].phone}' `;
            con.query(sql2,function(err,result){
                if(err) throw err;
                res.send({success:true});
            });
        }
            else{
                console.log('in lb else');
                sql3=`INSERT INTO crc_emp_lunchbox_details(company_id,emp_mobile_number,emp_nlb_dates,emp_fast_dates,emp_leave_dates) VALUES (1,${req.body[0].phone},"","",'${strdates}')`;
                con.query(sql3,function(err,result){
                    if(err) throw err;
                    res.send({success:true});
                });
            }
        });
        //------------------------------remaining days calculations and updations --------------------------
    
        /*let fdate = new Date(req.body[0].fdate),
                tdate = new Date(req.body[0].tdate);
        timeDifference = Math.abs(tdate.getTime() - fdate.getTime());
        let rdays = Math.ceil(timeDifference / (1000 * 3600 * 24))+1;
    
        sqlA=`SELECT * FROM crc_emp_leave_details WHERE emp_mobile_number='${req.body[0].phone}'`;
           con.query(sqlA,function(err,result){
               if(err) throw err;
               result.forEach(function(data){
                   let dbtleaves=data.emp_total_remaining_leaves;
                   let dbcasual=data.emp_casual_leaves_availed;
                   let dbsick=data.emp_sick_leaves_availed;
                   let remleave=dbtleaves-rdays;
                   let monthleav=data.emp_leaves_availed_this_month+rdays;
                   let yearleav=data.emp_leaves_availed_this_fin_year+rdays;
                   if(req.body[0].type==2){
                        let sklea=dbsick+rdays;     
                       sqlb=`UPDATE crc_emp_leave_details SET emp_sick_leaves_availed=${sklea},emp_total_remaining_leaves=${remleave},emp_leaves_availed_this_month=${monthleav},emp_leaves_availed_this_fin_year=${yearleav} WHERE emp_mobile_number='${req.body[0].phone}'`;
                       con.query(sqlb,function(err,result){
                           if(err) throw err;
                           res.send({success:true});
                       })
                   }else if(req.body[0].type==1){
                        let clea=dbcasual+rdays;
                        sqlc=`UPDATE crc_emp_leave_details SET emp_casual_leaves_availed='${clea}',emp_total_remaining_leaves='${remleave}',emp_leaves_availed_this_month=${monthleav},emp_leaves_availed_this_fin_year=${yearleav} WHERE emp_mobile_number='${req.body[0].phone}'`; 
                        con.query(sqlc,function(err,result){
                            if(err) throw err;
                            res.send({success:true});
                        })
                   }
               })   
           })*/
        //res.send({success:true});
    });
    





    /*-----------------Employee Details/Profile--------------------------------- */
    router.get('/empDetails',function(req,res){
        var sql=`SELECT *,SUBSTR(emp_dob, 1, 10) as emp_dob,SUBSTR(emp_doj, 1, 10) as emp_doj FROM crc_emp_details WHERE emp_mobile_number='${req.query.mob}'`;
    con.query(sql,function(err,result){
            if(err) throw err;
        res.send(result);
    });
    });
    




    /*---------------------Leave Status----------------------------------------- */
    router.get('/leaveStatus',function(req,res){
        var sql=`SELECT *,SUBSTR(emp_leave_start_date, 1, 10) as emp_leave_start_date,SUBSTR(emp_leave_end_date, 1, 10) as emp_leave_end_date FROM crc_emp_leave_requests INNER JOIN crc_leave_status_master ON crc_emp_leave_requests.emp_leave_status=crc_leave_status_master.leave_status_id INNER JOIN crc_leave_type_master ON crc_emp_leave_requests.emp_leave_type=crc_leave_type_master.leave_type_id WHERE emp_mobile_number='${req.query.phone}'`;
        con.query(sql,function(err,result){
            if(err) throw err;

            console.log('hiiii',result);
            res.send(result);
        });
        });
    





    /*-----------------------leaveReport------------------------------ */
    router.get('/leaveReport',function(req,res){
            var sql=`SELECT * FROM crc_emp_leave_details WHERE emp_mobile_number='${req.query.phone}'`;
            con.query(sql,function(err,result){
                if(err) throw err;
                res.send(result);
            });
        });
    






    /*------------------------On_Emp_update--------------------------------- */
    router.post('/empUpdate',function(req,res){
            
        if(req.body[0].phone!=req.body[0].mob)
        {
            sql1=`UPDATE crc_emp_leave_details SET emp_mobile_number='${req.body[0].phone}' WHERE emp_mobile_number='${req.body[0].mob}'`;
            sql2=`UPDATE crc_emp_leave_requests SET emp_mobile_number='${req.body[0].phone}' WHERE emp_mobile_number='${req.body[0].mob}'`;
            con.query(sql1,function(err,result){
                if(err) throw err;
            });
            con.query(sql2,function(err,result){
                if(err) throw err;
            });
        }
        sql=`UPDATE crc_emp_details SET emp_first_name='${req.body[0].fname}',emp_last_name='${req.body[0].lname}',emp_passcode='${req.body[0].password}',emp_email='${req.body[0].email}',emp_doj='${req.body[0].doj}',emp_dob='${req.body[0].dob}',emp_is_vegetarian='${req.body[0].isVeg}',emp_is_eggetarian='${req.body[0].isEgg}',emp_is_non_vegetarian='${req.body[0].isNonVeg}',emp_mobile_number='${req.body[0].phone}',emp_gender='${req.body[0].gender}' WHERE emp_mobile_number='${req.body[0].mob}'`;
        con.query(sql,function(err,result){
            if(err) throw err;
            res.send({success:true});
        });
           
        })
    
  

        module.exports = router;
   