var express    = require('express');
const router = express.Router();

var md5 = require('md5');
var app = express();
var con = require('../server');   //db connection from server.js file






/*------------------------Add Employee------------------------ */

router.post('/addEmp',function(req,res){
    sql=`INSERT INTO crc_emp_details(company_id,emp_mobile_number,emp_passcode,emp_first_name,emp_last_name,emp_email,emp_doj,emp_dob,emp_gender,emp_is_vegetarian,emp_is_eggetarian,emp_is_non_vegetarian,emp_role,emp_account_status) VALUES(1,${req.body[0].phone},'${md5(req.body[0].password)}','${req.body[0].fname}','${req.body[0].lname}','${req.body[0].email}','${req.body[0].doj}','${req.body[0].dob}',${req.body[0].gender},1,${req.body[0].isEgg},${req.body[0].isNonVeg},2,1)`;
       sql2=`INSERT INTO crc_emp_leave_details(company_id,emp_mobile_number,emp_total_eligible_leaves,emp_earned_leaves_availed,emp_sick_leaves_availed,emp_casual_leaves_availed,emp_total_remaining_leaves,emp_leaves_availed_this_month,emp_leaves_availed_this_fin_year) VALUES(1,${req.body[0].phone},24,0,0,0,24,0,0)`; 
       con.query(sql,function(err,result){
            if(err) throw err; 
        })
        con.query(sql2,function(err,result){
            if(err) throw err;
            res.send({success:true});
        })
 })

/*-------------------------- adLeaveRequest------------------------------*/

router.get('/adLeaveRequest',function(req,res){
    sql='SELECT *,SUBSTR(emp_leave_start_date, 1, 10) as emp_leave_start_date,SUBSTR(emp_leave_end_date, 1, 10) as emp_leave_end_date FROM crc_emp_leave_requests INNER JOIN crc_leave_type_master ON crc_emp_leave_requests.emp_leave_type=crc_leave_type_master.leave_type_id WHERE emp_leave_status=1';
        con.query(sql,function(err,result){
            if(err) throw err;
            res.send(result);
        })
 });



/*---------------------------adEmpDetails------------------------------- */

router.get('/adEmpDetails',function(req,res){
    sql='SELECT *,SUBSTR(emp_dob, 1, 10) as emp_dob,(case when emp_account_status = 1 then "Active" when emp_account_status = 2 then "Inactive" else emp_account_status end) as emp_account_status FROM crc_emp_details  INNER JOIN crc_emp_leave_details ON crc_emp_details.emp_mobile_number= crc_emp_leave_details.emp_mobile_number  where emp_role = 2';

/***
 * INNER JOIN emp_status_master ON crc_emp_details.emp_account_status=emp_status_master.status_id      //bcoz to show both active inactive
 */
    con.query(sql,function(err,result){
        if(err) throw err;

     //  console.log(result[0]);
        res.send(result);
    })
 });



/*------------------------deleteEmp------------------------------------------- */
router.get('/deleteEmp',function(req,res){
     console.log('in delete emp');
     sql=`DELETE FROM crc_emp_details WHERE emp_mobile_number=${req.query.phone}`;
     sql1=`DELETE FROM crc_emp_leave_details WHERE emp_mobile_number=${req.query.phone}`;
     con.query(sql,function(err,result){
         if(err) throw err;
         
     })
     con.query(sql1,function(err,result){
         if(err) throw err;
         res.send({success:true});
     })
 });



/*-----------------------adEmpStatus-------------------------- */
router.get('/adEmpStatus',function(req,res){
     console.log('Hello'+req.query.status);
     sql=`UPDATE crc_emp_details SET emp_account_status=${req.query.status},emp_role=${req.query.role} WHERE emp_mobile_number='${req.query.phone}'`;
     con.query(sql,function(err,result){
         if (err) throw err;
         res.send({success:true});
     })
 })



/*-------------------------get Employee-------------------------- */
router.get('/getEmp',function(req,res){

    

     sql=`SELECT *,SUBSTR(emp_dob, 1, 10) as emp_dob,SUBSTR(emp_doj, 1, 10) as emp_doj FROM crc_emp_details WHERE emp_mobile_number='${req.query.phone}'`;
     con.query(sql,function(err,result){
         if(err) throw err;
      
         res.send(result);
     })
 });



/*------------------------------- reject Request--------------------*/
/* router.get('/rejectReq',function(req,res){
     sql=`update leaverequest set msg='${req.query.msg}',status='Rejected' where (phone='${req.query.phone}' && fDate='${req.query.fdate}')`;
     con.query(sql,function(err,result){
         if(err) throw err;
         res.send(result);
     })
 })*/
/*-----------------------------Accept Request------------------------ */
/* router.get('/acceptReq',function(req,res){
     let fdate = new Date(req.query.fdate),
         tdate = new Date(req.query.tdate);
 timeDifference = Math.abs(tdate.getTime() - fdate.getTime());
 let rdays = Math.ceil(timeDifference / (1000 * 3600 * 24))+1;
 sql=`update leaverequest set msg='${req.query.msg}',status='Approved' where (phone='${req.query.phone}' && fDate='${req.query.fdate}')`;
 con.query(sql,function(err,result){
     if (err) throw err;
 })

 sql2=`select * from leavesreport where phone='${req.query.phone}'`;
    con.query(sql2,function(err,result){
        if(err) throw err;
        result.forEach(function(data){
            let dbtleaves=parseInt(data.Remaining_L,10);
            let dbcasual=parseInt(data.Casual_L,10);
            let dbsick=parseInt(data.Sick_L,10);
            let remleave=dbtleaves-rdays;
            if(req.query.type=='SL'){
                 let sklea=dbsick+rdays;
                
                sql4=`update leavesreport set Sick_L='${sklea}',Remaining_L='${remleave}' where phone='${req.query.phone}'`;
                con.query(sql4,function(err,result){
                    if(err) throw err;
                    res.send({success:true});
                })
            }else if(req.query.type=='CL'){
                 let clea=dbcasual+rdays;
                 sql5=`update leavesreport set Casual_L='${clea}',Remaining_L='${remleave}' where phone='${req.query.phone}'`; 
                 con.query(sql5,function(err,result){
                     if(err) throw err;
                     res.send({success:true});
                 })
            }
        })   
    })
 });*/


 
router.get('/noAvailChk',function(req,res){
     sql=`SELECT emp_mobile_number FROM crc_emp_details`;
     con.query(sql,function(err,result){
         if(err) throw err;
         result.forEach(function(data){
             if(data.emp_mobile_number==req.query.phone){
                 res.send({success:false});   
             }
         })
     })
 })

 module.exports = router;
   